'use strict';

// ================================================================
// Controllers
// ================================================================

angular.module('appfoco.controllers', [])

// App
// ================================================================
.controller('AppCtrl', ['$scope', '$ionicModal', '$timeout', '$ionicLoading', '$ionicPopup', '$http', function ($scope, $ionicModal, $timeout, $ionicLoading, $ionicPopup, $http) {	
	if(localStorage['logado_foco']){
		if(localStorage['logado_foco'] == false){
			window.location = "#/app/login";
		}else{}
	}else {
		window.location = "#/app/login";
	}

	$scope.login = function(data) {
		localStorage.setItem("logado_foco", true);
    	localStorage.setItem("idCli_foco", data.cli_id);
    	localStorage.setItem("idUsu_foco", data.id_usu);
    	localStorage.setItem("tpUsu_foco", data.tp_usu);
    	window.location = "#/app/home";
    	location.reload();
	};
	$scope.logout = function() {
		localStorage.setItem("logado_foco", false);
    	localStorage.setItem("idCli_foco", ' ');
    	localStorage.setItem("idUsu_foco", ' ');
    	localStorage.setItem("tpUsu_foco", ' ');
    	window.location = "#/app/login";
    	location.reload();
	};
	$scope.sair = function() {
		$ionicPopup.confirm({
				title: 'Sair',
				template: 'Deseja sair do aplicativo?'
			}).then(function(res) {
				if(res) {
					$scope.logout();
				} else {}
			});
	};

	$scope.loadShow = function() {
	    $ionicLoading.show({
	      	content: 'Carregando',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 250,
			showDelay: 0
	    });
	};
	$scope.loadHide = function(tmp) {
		$timeout(function () {
			$ionicLoading.hide();
		}, tmp);
	};

	$scope.showFooter = false;
	$scope.toggleFooter = function() {
		$scope.showFooter = !$scope.showFooter;
	}

	$scope.ativaGuede = function(idVei, Mod) {
		$scope.loadShow();
        $http({
            method : 'POST',
            url : 'http://70.38.68.5/app_trackingcenter/services/services.php?action=MODOGUEDE',
            data : {'id_vei': idVei, 'uMod': Mod},
            headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        })
        .success(function(data){
	        $scope.loadHide();
	        console.log(data)
	    })
	    .error(function(err){
	    	$scope.loadHide();
	    	"ERR", console.log(err);
	    })
	}
}])

// Login
// ================================================================
.controller('LoginCtrl', ['$scope', '$ionicSideMenuDelegate', '$http', '$ionicPopup', function ($scope, $ionicSideMenuDelegate, $http, $ionicPopup) {
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.loadShow();
	$scope.loadHide(1000);

	$scope.dataLogin = {};
	$scope.resultLogin;
    $scope.logar = function() {
    	$scope.loadShow();
        $http({
            method : 'POST',
            url : 'http://70.38.68.5/app_trackingcenter/services/services.php?action=LOGIN',
            data : $scope.dataLogin,
            headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        })
        .success(function(data){
	        $scope.loadHide();
	        Auth(data.dados[0]);
	    })
	    .error(function(err){
	    	$scope.loadHide();
	    	"ERR", console.log(err);
	    })
    }
    function Auth(data) {
		if(data.status === 'true') {
			$scope.login(data);
		}else {
			$ionicPopup.alert({
				title: 'Falha ao Logar...',
				template: 'Verifique se sua senha e tente novamente!'
			});
		}
	}
}])

.controller('HomeCtrl',['$scope', 'ClientesService', function ($scope, ClientesService) {
	$scope.loadShow();
	ClientesService.get().$promise.then(function (data) {
		var clientes = data.dados;
		$scope.clientes = clientes;
		$scope.loadHide(2000);
	});
}])

.controller('ClienteCtrl',['$scope', '$stateParams', 'ClienteService', '$ionicActionSheet', function ($scope, $stateParams, ClienteService, $ionicActionSheet) {
	$scope.loadShow();
	$scope.cliente = $stateParams.clienteId;
	ClienteService.get({ id: $stateParams.clienteId }).$promise.then(function (data) {
		var veiculos = data.dados;
		$scope.veiculos = veiculos;
		$scope.loadHide(2000);
		console.log(veiculos);
	});

	$scope.doRefresh = function() {
		ClienteService.get({ id: $stateParams.clienteId }).$promise.then(function (data) {
			var veiculos = data.dados;
			$scope.veiculos = veiculos;
		});
		$scope.$broadcast('scroll.refreshComplete');
		$scope.$apply()
	};

	$scope.showAction = function(id_vei, uMod, vei, id, lat, lng) {
		var tipoUser = localStorage['tpUsu_foco'];

		if(tipoUser == 1){
			$ionicActionSheet.show({
				titleText: vei,
				cancelText: 'Cancelar',
				buttons: [
			        { text: '<i class="icon ion-ios-location"></i> Ver no Mapa' },
			        { text: '<i class="icon ion-flash"></i> Ativar/Desativar' },
			    ],
				buttonClicked: function(index) {
					switch (index){
						case 0 :
							window.location = "#/app/mapa/"+id+"/"+lat+"/"+lng;
							return true;
						case 1 :
							$scope.ativaGuede(id_vei, uMod);
							return true;
					}
			    }
			});
		}else{
			$ionicActionSheet.show({
				titleText: vei,
				cancelText: 'Cancelar',
				buttons: [
			        { text: '<i class="icon ion-ios-location"></i> Ver no Mapa' },
			        //{ text: '<i class="icon ion-flash"></i> Ativar/Desativar' },
			    ],
				buttonClicked: function(index) {
					switch (index){
						case 0 :
							window.location = "#/app/mapa/"+id+"/"+lat+"/"+lng;
							return true;
					}
			    }
			});
		}

	}
}])

.controller('VeiculoCtrl',['$scope', '$stateParams', 'VeiculoService', function ($scope, $stateParams, VeiculoService) {
	$scope.loadShow();

	$scope.latitude = $stateParams.latitude;
	$scope.longitude = $stateParams.longitude;
	
	VeiculoService.get({ id: $stateParams.veiculoId }).$promise.then(function (data) {
		var veiculo = data.dados[0];
		$scope.veiculo = veiculo;
		$scope.loadHide(2000);
	});

	$scope.doRefresh = function() {
		$scope.loadShow();
		VeiculoService.get({ id: $stateParams.veiculoId }).$promise.then(function (data) {
			var veiculo = data.dados[0];
			$scope.veiculo = veiculo;
			$scope.latitude = veiculo.uLat;
			$scope.longitude = veiculo.uLng;
			$scope.loadHide(2000);
		});

		//$scope.$broadcast('scroll.refreshComplete');
		//$scope.$apply()
	};
}]);
