'use strict';

// ================================================================
// Directives
// ================================================================

angular.module('appfoco.directives', [])

.directive('map', function() {
  return {
    restrict: 'E',
    scope: {
      //onCreate: '&',
      latitude: '@',
      longitude: '@',
    },
    link: function ($scope, $element, $attr) {
      function initialize() {
        var myLatlng = new google.maps.LatLng($scope.latitude, $scope.longitude);

        var mapOptions = {
          center: myLatlng,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        };
        var map = new google.maps.Map($element[0], mapOptions);

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: ''
        });
  
        //$scope.onCreate({map: map});
        google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
          e.preventDefault();
          return false;
        });
      }

      if (document.readyState === "complete") {
        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
});