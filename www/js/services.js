'use strict';

// ================================================================
// Services
// ================================================================

angular.module('appfoco.services', [])

  .value('version', '1.0.0')


  // API
  .factory('API', function ($resource) {
    return 'http://70.38.68.5/app_trackingcenter/services/services.php?';
  })

  // Clientes
  .factory('ClientesService', function ($resource, API) {
    var idCliente = localStorage['idCli_foco'];
    return $resource(API + 'cliente='+idCliente+'&action=BUSCA_CLIENTES', {}, {
      update: {
        method: 'PUT',
        url: API + 'cliente='+idCliente+'&action=BUSCA_CLIENTES'
      }
    });
  })

  // Veículos
  .factory('ClienteService', function ($resource, API) {
    return $resource(API + 'action=BUSCA_VEICULOS&cliente=:id', { id: '@id' }, {
      update: {
        method: 'PUT',
        url: API + 'action=BUSCA_VEICULOS&cliente=:id',
        params: { id: '@id' }
      }
    });
  })

  // Veículo
  .factory('VeiculoService', function ($resource, API) {
    return $resource(API + 'action=BUSCA_VEICULO&veiculo=:id', { id: '@id' }, {
      update: {
        method: 'PUT',
        url: API + 'action=BUSCA_VEICULO&veiculo=:id',
        params: { id: '@id' }
      }
    });
  })