'use strict';

// ================================================================
// Configurations
// ================================================================

angular.module('appfoco', [
      'ionic', 
      'ngResource',
      'appfoco.controllers',
      'appfoco.services',
      'appfoco.directives'
    ])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  })

  .state('app.home', {
    url: "/home",
    views: {
      'menuContent': {
        templateUrl: "templates/home.html",
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.cliente', {
    url: "/cliente/:clienteId",
    views: {
      'menuContent': {
        templateUrl: "templates/cliente.html",
        controller: 'ClienteCtrl'
      }
    }
  })

  .state('app.mapa', {
    url: "/mapa/:veiculoId/:latitude/:longitude",
    views: {
      'menuContent': {
        templateUrl: "templates/mapa.html",
        controller: 'VeiculoCtrl'
      }
    }
  });

  $urlRouterProvider.otherwise('/app/home');
});
